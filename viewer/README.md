Model 3D Viewer
===============

A web based [Model 3D file viewer](https://bztsrc.gitlab.io/model3d/viewer), provides the same functionality as the
[m3dview](https://gitlab.com/bztsrc/model3d/tree/master/m3dview) utility, but uses WebGL interface with GLES and compiled
for WebAssembly.

TODO: you must reload the page before you could load a new model.
