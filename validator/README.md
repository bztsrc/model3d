Model 3D Validator
==================

A web based [Model 3D file validator](https://bztsrc.gitlab.io/model3d/validator), provides the same functionality as the
`-d`, `-dd`, `-ddd` and `-D` flags of the [m3dconv](https://gitlab.com/bztsrc/model3d/tree/master/m3dconv#dumping) utility.
